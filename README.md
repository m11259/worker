# msg_guard_worker

## Description
This project is defined to work inside MSG-Guard-Service.
Worker built on top of Redis to copy data from Google Sheets into the storage and process it.

Current worker tasks:
- From the __base__ sheet (need reading access)
    - copy all addresses
    - copy adresses that are banned (**badass** address)
    - copy the following data
        - phone numbers associated to the address
        - vehicle identification numbers associated to the address
- From the __payments__ sheet (need reading access)
    - aggregate (sum) payments to the address during the last 3 months
    - store unpaid months
- Disables users in postgres database at the beginning of the month and reactivates them if they paid enough during the last 3 months
- Write additional data (requests) to the appropriate Google Sheet
- Deny obsolete (that live longer than 24 hours) requests (that are stored in postgres database) at 00:00 UTC.

## Installation
Use docker image `registry.gitlab.com/aipyth/msg_guard_worker`.

## Usage
The worker needs the following configuration:
### Redis connection configuration
Create such environmental variables:
| Name                  | Contents                    |
|-----------------------|-----------------------------|
| WORKER_REDIS_ADDRESS  | Url address to access redis |
| WORKER_REDIS_PASSWORD | Password to acess redis     |

__Worker uses 0th redis database.__

### Postgres connetion configuration
Create such environmental variables:
| Name          | Contents                       |
|---------------|--------------------------------|
| POSTGRES_URL  | Url address to access postgres |

### Google Account Credentials
You need service account. Provide access to your google sheets to this account.
Bind file `credentials.json` containing key for this account to `/credentials.json`.

### Config

Also you need to bind your config file `config.yml` to `/config.yml`.

Through the whole project words 'address' and 'flat' have the same meaning.
That's because firstly it was written for one house with one address and
many flat and now it is adapted for whole living squares of the city.

### Redis

Data written to redis:
| Key                        | Data type | Additional information |
|----------------------------|-----------|------------------------      |
| vin:<address>              | set       |                              |
| flatPhoneNumbers:<address> | set       |                              |
| flats                      | set       |                              |
| badasses                   | set       |                              |
| last-months-payments       | hashmap   | keys maps to sum of payments |
| unpaid:<address>           | set       |                              |
| disabled-month             | key       | used to remember the last month of disabling |
| new-requests               | list      | add to this array for worker to add the payload separated by `;` to __requests__ Google Sheet |


### Postgres

Used query to disable users
```sql
UPDATE users SET activated = FALSE WHERE address_confirmed = TRUE;
```
Used query to reactivate user back:
```sql
UPDATE users SET activated = TRUE WHERE address = $1 AND address_confirmed = TRUE;
```
Used query to disable specific address
```sql
UPDATE users SET activated = FALSE WHERE address = $1;
```
And reactivate it back
```sql
UPDATE users SET activated = TRUE WHERE address = $1 AND activated = FALSE;
```
Used query to deny requests
```sql
UPDATE requests SET accepted=-1, reviewed_at=now() WHERE accepted=0 AND created_at < NOW() - INTERVAL '1 day' RETURNING id;
```
In field `accepted` value `0` means that request is still not marked as done, `-1` that it is denied.

Also `req_deny` event is sent with id of the request
```sql
select pg_notify('req_deny', $1);
```



/*******************************************************
* Copyright (C) 2021-2022 Ivan Zhytkevych <aipyth.func@gmail.com>
*
* This file is part of msg-guard-service.
*
* msg-guard-service can not be copied and/or distributed without the express
* permission of Ivan Zhytkevych
*******************************************************/
