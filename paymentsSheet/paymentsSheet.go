/*******************************************************
* Copyright (C) 2021-2022 Ivan Zhytkevych <aipyth.func@gmail.com>
*
* This file is part of msg-guard-service.
*
* msg-guard-service can not be copied and/or distributed without the express
* permission of Ivan Zhytkevych
*******************************************************/
package paymentsSheet

import (
	"context"
	"fmt"
	"log"
	"strings"
    "regexp"
	"time"

    "worker/config"

	"google.golang.org/api/option"
	"google.golang.org/api/sheets/v4"
)

type PaymentsSheet struct {
	SheetsService *sheets.Service
	sheetId       string
    config *config.Config

	sheetValues     *sheets.ValueRange
	lastSheetUpdate time.Time
}

func NewPaymentsSheet(conf *config.Config) *PaymentsSheet {
	const jsonPath = "./credentials.json"
	pc := &PaymentsSheet{}
    pc.config = conf
	pc.sheetId = conf.Payments.SheetId
	if err := pc.createService(jsonPath); err != nil {
		log.Fatal(err)
	}
	return pc
}

func (p *PaymentsSheet) createService(credentialsPath string) error {
	var err error
	ctx := context.Background()
	p.SheetsService, err = sheets.NewService(ctx, option.WithCredentialsFile(credentialsPath))
	return err
}

func (p *PaymentsSheet) getAllSheet() (*sheets.ValueRange, error) {
	var sheetFreshnessLimit = time.Minute * time.Duration(p.config.Payments.SheetDataLimit)
	if p.lastSheetUpdate.Add(sheetFreshnessLimit).After(time.Now()) {
		return p.sheetValues, nil
	}

    readRange := p.config.Payments.ReadRange
	resp, err := p.SheetsService.Spreadsheets.Values.Get(p.sheetId, readRange).Do()
	if err != nil {
		return resp, err
	}
	p.lastSheetUpdate = time.Now()
	p.sheetValues = resp
	return resp, err
}

func (p *PaymentsSheet) GetYearMonthColIndex(year int, month int) (yearIdx int, monthIdx int) {
	resp, err := p.getAllSheet()
	if err != nil {
		log.Println(err)
		return
	}
    yearStartCol := p.config.Payments.YearStartCol
	values := resp.Values[p.config.Payments.YearRow]
	for i := yearStartCol; i < len(values); i++ {
		v, ok := values[i].(string)
		if ok && strings.Contains(v, fmt.Sprint(year)) {
			yearIdx = i
			break
		}
	}
	monthIdx = yearIdx + month + p.config.Payments.YearMonthIndexDelta
	return
}

func (p *PaymentsSheet) GetMonthYearPayments(year int, month int) [][]string {
	flats := make([][]string, 0)

    flatsRowStart := p.config.Payments.FlatsRowStart
    flatNameCol := p.config.Payments.FlatNameCol
    flatRegexp, err := regexp.Compile(p.config.Payments.FlatRegexp)
    if err != nil {
        log.Fatal("[PaymentsSheet] Invalid regexp:", err)
    }

	resp, err := p.getAllSheet()
	if err != nil {
		log.Println(err)
		return flats
	}

	_, monthCol := p.GetYearMonthColIndex(year, month)

	var flatName, flatPayment string
	for i := flatsRowStart; i < len(resp.Values); i++ {
        if len(resp.Values[i]) < flatNameCol + 1 {
            // log.Printf("[PaymentsSheet] cannot get address name from column %d. %d row length is less than it\n",
            //     flatNameCol, i)
            continue
        }
		if len(resp.Values[i]) < monthCol+1 {
			flatName = resp.Values[i][flatNameCol].(string)
			flatPayment = "0"
		} else {
			flatName = resp.Values[i][flatNameCol].(string)
			flatPayment = resp.Values[i][monthCol].(string)
		}

        flatSubmatch := flatRegexp.FindStringSubmatch(flatName)
        if len(flatSubmatch) < 2 {
			continue
		}
        flatName = flatSubmatch[1]
        flatPayment = strings.Replace(flatPayment, ",", ".", 1)
		flats = append(flats, []string{
			flatName,
			flatPayment,
		})
	}

	return flats
}
