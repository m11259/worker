/*******************************************************
* Copyright (C) 2021-2022 Ivan Zhytkevych <aipyth.func@gmail.com>
*
* This file is part of msg-guard-service.
*
* msg-guard-service can not be copied and/or distributed without the express
* permission of Ivan Zhytkevych
*******************************************************/
package baseSheet

import (
	"context"
	"log"
    "strings"
	"regexp"
	"time"
	"worker/config"

	"google.golang.org/api/option"
	"google.golang.org/api/sheets/v4"
)

var VINRegex = regexp.MustCompile("([a-zA-Zа-яА-ЯІі]{2}[0-9]{4}[a-zA-Zа-яА-ЯіІ]{2})")
var PhoneNumberRegex = regexp.MustCompile("(0[0-9]{9})")

type BaseSheet struct {
	SheetsService *sheets.Service
	sheetId       string
    config *config.Config

	sheetValues     [][]interface{}
	lastSheetUpdate time.Time
}

func NewBaseSheet(conf *config.Config) *BaseSheet {
	const jsonPath = "./credentials.json"
	pc := &BaseSheet{}
    pc.config = conf
	pc.sheetId = conf.Base.SheetId
	if err := pc.createService(jsonPath); err != nil {
		log.Fatal(err)
	}
	return pc
}

func (p *BaseSheet) createService(credentialsPath string) error {
	var err error
	ctx := context.Background()
	p.SheetsService, err = sheets.NewService(ctx, option.WithCredentialsFile(credentialsPath))
	return err
}

func (p *BaseSheet) getAllSheet() ([][]interface{}, error) {
	var sheetFreshnessLimit = time.Minute * time.Duration(p.config.Base.SheetDataLimit)
	if p.lastSheetUpdate.Add(sheetFreshnessLimit).After(time.Now()) {
		return p.sheetValues, nil
	}

    p.sheetValues = make([][]interface{}, 0)
    for _, readRange := range p.config.Base.ReadRange {
        resp, err := p.SheetsService.Spreadsheets.Values.Get(p.sheetId, readRange).Do()
        if err != nil {
            return p.sheetValues, err
        }
        p.sheetValues = append(p.sheetValues, resp.Values...)
    }
	p.lastSheetUpdate = time.Now()
	return p.sheetValues, nil
}

func (b *BaseSheet) readColumns(startRow int, keyColumn int, readColumns []int, values [][]interface{}, keyRegexp regexp.Regexp, valueRegexp regexp.Regexp) map[string][]string {
	var key string
	results := make(map[string][]string)
	for i := startRow; i < len(values); i++ {
		for _, readCol := range readColumns {
			if len(values[i]) < readCol + 1 {
                // log.Printf("row %d small for size %d\n", i, readCol)
				continue
			}

			key = values[i][keyColumn].(string)
            keySubmatch := keyRegexp.FindStringSubmatch(key)
            if len(keySubmatch) < 2 {
                // log.Println("no submatch", key)
                continue
            }
            key = keySubmatch[1]

			readRaw := values[i][readCol].(string)
			if _, ok := results[key]; ok {
				results[key] = append(results[key], valueRegexp.FindAllString(readRaw, -1)...)
			} else {
				results[key] = valueRegexp.FindAllString(readRaw, -1)
			}
		}
	}
	return results
}

func (b *BaseSheet) GetVINs() map[string][]string {
	values, err := b.getAllSheet()
	if err != nil {
		log.Println("[BaseSheet] error getting sheet values", err)
		return map[string][]string{}
	}

    flatsRowStart := b.config.Base.FlatsRowStart
    flatNameCol := b.config.Base.FlatNameCol
    flatRegexp, err := regexp.Compile(b.config.Base.FlatRegexp)
    if err != nil {
        log.Fatal("[BaseSheet] Invalid regexp:", err)
    }
    vinCols := b.config.Base.VINCols

	return b.readColumns(flatsRowStart, flatNameCol, vinCols, values, *flatRegexp, *VINRegex)
}

func (b *BaseSheet) GetPhoneNumbers() map[string][]string {
	values, err := b.getAllSheet()
	if err != nil {
		log.Println("[BaseSheet] error getting sheet values", err)
		return map[string][]string{}
	}

    flatsRowStart := b.config.Base.FlatsRowStart
    flatNameCol := b.config.Base.FlatNameCol
    phoneNumberCols := b.config.Base.PhoneNumberCols
    flatRegexp, err := regexp.Compile(b.config.Base.FlatRegexp)
    if err != nil {
        log.Fatal("[BaseSheet] Invalid regexp:", err)
    }

	return b.readColumns(flatsRowStart, flatNameCol, phoneNumberCols, values, *flatRegexp, *PhoneNumberRegex)
}

func (b *BaseSheet) GetFlats() []string {
	values, err := b.getAllSheet()
	if err != nil {
		log.Println("[BaseSheet] error getting sheet values", err)
		return []string{}
	}

    flatsRowStart := b.config.Base.FlatsRowStart
    flatNameCol := b.config.Base.FlatNameCol
    flatRegexp, err := regexp.Compile(b.config.Base.FlatRegexp)
    if err != nil {
        log.Fatal("[BaseSheet] Invalid regexp:", err)
    }

	var flat string
	flats := make([]string, 0)
	for i := flatsRowStart; i < len(values); i++ {
        if len(values[i]) < flatNameCol+1 {
            continue
        }

        flat = values[i][flatNameCol].(string)
        flatSubmatch := flatRegexp.FindStringSubmatch(flat)
        if len(flatSubmatch) < 2 {
            continue
        }
        flat = flatSubmatch[1]
        flats = append(flats, flat)
	}
	return flats
}

func (p *BaseSheet) GetBadassAddresses() []string {
    flats := make([]string, 0)
    values, err := p.getAllSheet()
    if err != nil {
        log.Println(err)
        return []string{}
    }
    
    flatRegexp, err := regexp.Compile(p.config.Base.FlatRegexp)
    if err != nil {
        log.Fatal("[baseSheet] Invalid regexp:", err)
    }
    flatsRowStart := p.config.Base.FlatsRowStart
    flatNameCol := p.config.Base.FlatNameCol
    var flatName string
    var badass string
    for i := flatsRowStart; i < len(values); i++ {
        if len(values[i]) < flatNameCol + 1 {
            continue
        }
        flatName = values[i][flatNameCol].(string)
        for _, column := range p.config.Base.BadassColumns {
            if len(values) < column + 1 {
                log.Println("[BaseSheet] cannot reach badass column", column)
                continue
            }
            badass = values[i][column].(string)
            flatSubmatch := flatRegexp.FindStringSubmatch(flatName)
            if len(flatSubmatch) < 2 {
                continue
            }
            flatName = flatSubmatch[1]
            if !strings.Contains(badass, "1") {
                continue
            }
            flats = append(flats, flatName)
        }
    }
    return flats
}

func (p *BaseSheet) GetDeactivated() []string {
    flats := make([]string, 0)
    values, err := p.getAllSheet()
    if err != nil {
        log.Println(err)
        return []string{}
    }
    
    flatRegexp, err := regexp.Compile(p.config.Base.FlatRegexp)
    if err != nil {
        log.Fatal("[baseSheet] Invalid regexp:", err)
    }
    flatsRowStart := p.config.Base.FlatsRowStart
    flatNameCol := p.config.Base.FlatNameCol
    var flatName string
    var deactive string
    for i := flatsRowStart; i < len(values); i++ {
        if len(values[i]) < flatNameCol + 1 {
            continue
        }
        flatName = values[i][flatNameCol].(string)
        for _, column := range p.config.Base.DisableColumns {
            if len(values) < column + 1 {
                log.Println("[BaseSheet] cannot reach disable column", column)
                continue
            }
            deactive = values[i][column].(string)
            flatSubmatch := flatRegexp.FindStringSubmatch(flatName)
            if len(flatSubmatch) < 2 {
                continue
            }
            flatName = flatSubmatch[1]
            if !strings.Contains(deactive, "1") {
                continue
            }
            flats = append(flats, flatName)
        }
    }
    return flats
    
}
