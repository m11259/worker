FROM golang:1.16 AS builder

WORKDIR /app
COPY . .

ENV GO111MODULE=on

RUN CGO_ENABLED=0 GOOS=linux go build -installsuffix cgo -o main .
# RUN GOOS=linux go build -o main .


FROM scratch

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /app/main /

CMD ["/main"]

