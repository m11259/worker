/*******************************************************
* Copyright (C) 2021-2022 Ivan Zhytkevych <aipyth.func@gmail.com>
*
* This file is part of msg-guard-service.
*
* msg-guard-service can not be copied and/or distributed without the express
* permission of Ivan Zhytkevych
*******************************************************/
package redisWorker

import (
	"context"
	"log"
	"os"
	"time"
	"worker/baseSheet"
	"worker/config"
	"worker/paymentsSheet"
	requestssheet "worker/requestsSheet"

	redis "github.com/go-redis/redis/v8"

	"database/sql"

	_ "github.com/lib/pq"
)

var redisAddr = os.Getenv("WORKER_REDIS_ADDRESS")
var redisPass = os.Getenv("WORKER_REDIS_PASSWORD")

type Worker struct {
	Config         *config.Config
	postgresClient *sql.DB
	redisClient    *redis.Client
	paymentsSheet  *paymentsSheet.PaymentsSheet
	baseSheet      *baseSheet.BaseSheet
	requestsSheet  *requestssheet.RequestsSheet
}

type DumbTicker struct {
    C chan int
}

func initRedisConn() *redis.Client {
	client := redis.NewClient(&redis.Options{
		Addr:     redisAddr,
		Password: redisPass,
		DB:       0,
	})
	_, err := client.Ping(context.Background()).Result()
	if err != nil {
        log.Fatal("error pinging redis:", err)
	}
	return client
}

func initPostgresConn() *sql.DB {
	db, err := sql.Open("postgres", os.Getenv("POSTGRES_URL"))
	if err != nil {
		log.Fatalln(err)
	}
	return db
}

func NewWorker() *Worker {
	conf := config.GetConfig()
	log.Println("[Worker] loaded config.yml successfuly.")
    log.Printf("[Worker] Config: %+v\n", conf)
	worker := &Worker{
		Config: conf,
	}
	worker.paymentsSheet = paymentsSheet.NewPaymentsSheet(conf)
	worker.baseSheet = baseSheet.NewBaseSheet(conf)
	worker.requestsSheet = requestssheet.NewRequestsSheet(conf)
	worker.redisClient = initRedisConn()
	worker.postgresClient = initPostgresConn()
	return worker
}

func (w *Worker) Run() {
	updateCurrentMonthPeriod := time.Second * time.Duration(w.Config.Timers.LastMonthsPayments)
	updateUnpaidPeriod       := time.Second * time.Duration(w.Config.Timers.Unpaid)
	updateVINPeriod          := time.Second * time.Duration(w.Config.Timers.VIN)
	updatePhoneNumbersPeriod := time.Second * time.Duration(w.Config.Timers.PhoneNumbers)
	processNewRequestPeriod  := time.Second * time.Duration(w.Config.Timers.NewRequests)
    updateFlatsPeriod        := time.Second * time.Duration(w.Config.Timers.Flats)
    updateBadass             := time.Second * time.Duration(w.Config.Timers.Badass)
    updateDisable            := time.Second * time.Duration(w.Config.Timers.Disable)

    // for tickers to be able to initialize make periods positive
	if updateCurrentMonthPeriod == 0 {
        updateCurrentMonthPeriod = time.Second
	}
	if updateUnpaidPeriod == 0 {
        updateUnpaidPeriod = time.Second
	}
	if updateVINPeriod == 0 {
        updateVINPeriod = time.Second
	}
	if updatePhoneNumbersPeriod == 0 {
        updatePhoneNumbersPeriod = time.Second
	}
	if processNewRequestPeriod == 0 {
        processNewRequestPeriod = time.Second
	}
	if updateFlatsPeriod == 0 {
        updateFlatsPeriod = time.Second
	}
    if updateBadass == 0 {
        updateBadass = time.Second
    }
    if updateDisable == 0 {
        updateDisable = time.Second
    }

    updateCurrentMonthTicker := time.NewTicker(updateCurrentMonthPeriod)
	updateUnpaidTicker       := time.NewTicker(updateUnpaidPeriod)
	updateVINTicker          := time.NewTicker(updateVINPeriod)
	updatePhoneNumbersTicker := time.NewTicker(updatePhoneNumbersPeriod)
	processNewRequestTicker  := time.NewTicker(processNewRequestPeriod)
    updateFlatsTicker        := time.NewTicker(updateFlatsPeriod)
    updateBadassTicker       := time.NewTicker(updateBadass)
    updateDisableTicker      := time.NewTicker(updateDisable)

	// if timer period in config is 0 - we must not proceed to this event
	// first tick of tickers is not as started so do by hand first update
	if w.Config.Timers.LastMonthsPayments == 0 || w.Config.Payments.SheetId == "" {
        log.Println("Stopping update last month payments ticker")
        updateCurrentMonthTicker.Stop()
	} else {
		w.updateCurrentMonthPaymentsInfo()
	}

	if w.Config.Timers.Unpaid == 0 || w.Config.Payments.SheetId == "" {
        log.Println("Stopping update unpaid ticker")
		updateUnpaidTicker.Stop()
	} else {
		w.updateUnpaidMonthInfo()
	}

	if w.Config.Timers.VIN == 0 || w.Config.Base.SheetId == "" {
        log.Println("Stopping update vin ticker")
		updateVINTicker.Stop()
	} else {
		w.updateVIN()
	}

	if w.Config.Timers.PhoneNumbers == 0 || w.Config.Base.SheetId == "" {
        log.Println("Stopping update phone numbers ticker")
		updatePhoneNumbersTicker.Stop()
	} else {
		w.updatePhoneNumbers()
	}

	if w.Config.Timers.NewRequests == 0 || w.Config.Requests.SheetId == "" {
        log.Println("Stopping new requests ticker")
		processNewRequestTicker.Stop()
	} else {
		w.ProcessNewRequests()
	}

    if w.Config.Timers.Flats == 0 || w.Config.Base.SheetId == ""{
        log.Println("Stopping flats ticker")
        updateFlatsTicker.Stop()
    } else {
        w.updateFlats()
    }

    if w.Config.Timers.Badass == 0 || w.Config.Base.SheetId == "" {
        log.Println("Stopping badass ticker")
        updateBadassTicker.Stop()
    } else {
        w.updateBadass()
    }

    if w.Config.Timers.Disable == 0 || w.Config.Base.SheetId == "" {
        log.Println("Stopping `disable` ticker")
        updateDisableTicker.Stop()
    } else {
        w.updateDeactivated()
    }

	// Timer to deny handling active requests
	setUpDenyActiveRequestsTimer := func() *time.Timer {
		delta := time.Hour * time.Duration(w.Config.Timers.DenyRequests)
        if delta == 0 {
            delta = time.Hour
        }
		current := time.Now()
		midnight := current.Truncate(delta).Add(delta)
		untilTick := midnight.Sub(current)
		return time.NewTimer(untilTick)
	}
	denyRequestsTimer := setUpDenyActiveRequestsTimer()
	if w.Config.Timers.DenyRequests == 0 {
        log.Println("Stopping `deny requests` ticker")
		denyRequestsTimer.Stop()
	}

	// Main loop
	for {
		select {
		case <-updateCurrentMonthTicker.C:
			w.disableAllClientIfNeeded()
			w.updateCurrentMonthPaymentsInfo()
		case <-updateUnpaidTicker.C:
			w.updateUnpaidMonthInfo()
		case <-updateVINTicker.C:
			w.updateVIN()
		case <-updatePhoneNumbersTicker.C:
			w.updatePhoneNumbers()
		case <-processNewRequestTicker.C:
            if w.Config.Timers.NewRequests == 0 {
                continue
            }
			w.ProcessNewRequests()
		case <-denyRequestsTimer.C:
			w.DenyOldActiveRequests()
			denyRequestsTimer = setUpDenyActiveRequestsTimer()
        case <-updateFlatsTicker.C:
            w.updateFlats()
        case <-updateBadassTicker.C:
            w.updateBadass()
        case <-updateDisableTicker.C:
            w.updateDeactivated()
		}
	}
}
