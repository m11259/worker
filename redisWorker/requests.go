/*******************************************************
* Copyright (C) 2021-2022 Ivan Zhytkevych <aipyth.func@gmail.com>
*
* This file is part of msg-guard-service.
*
* msg-guard-service can not be copied and/or distributed without the express
* permission of Ivan Zhytkevych
*******************************************************/
package redisWorker

import (
	"context"
	"log"
	"strings"
	"time"
)

const newRequestsKey = "new-requests"
const requestPayloadSeparator = ";"

func (w *Worker) ProcessNewRequests() {
	for {
		request, err := w.redisClient.LPop(context.Background(), newRequestsKey).Result()
		if err != nil && err.Error() != "redis: nil" {
			log.Println("[Worker:ProcessNewRequests] Error:", err)
			break
		}
		if request == "" {
			break
		}
		splittedData := strings.Split(request, requestPayloadSeparator)
        data := make([]interface{}, len(splittedData))
        for index, value := range splittedData {
            data[index] = value
        }
		err = w.requestsSheet.Add(data...)
		if err != nil {
			log.Printf("[Worker:ProcessNewRequests] Schedule request back and initializing 20 seconds wait due to error:\n\t%v\n", err)
			_, err := w.redisClient.LPush(context.Background(), newRequestsKey, request).Result()
			if err != nil {
				log.Printf("[worker:ProcessNewRequests] Request %s was not pushed back: %v\n", request, err)
			}
			time.Sleep(20 * time.Second)
		}
	}
}

func (w *Worker) DenyOldActiveRequests() {
	const query = "UPDATE requests SET accepted=-1, reviewed_at=now() WHERE accepted=0 AND created_at < NOW() - INTERVAL '1 day' RETURNING id;"
	rows, err := w.postgresClient.Query(query)
	if err != nil {
		log.Printf("[Worker] Error denying active requests. %v", err)
		return
	}
	log.Printf("[Worker] Successfuly denied all rest requests.\n")
	// send notification of deny
	go func() {
		const denyQuery = "select pg_notify('req_deny', $1);"
		defer rows.Close()
		var reqId uint32
		for rows.Next() {
			if err := rows.Scan(&reqId); err != nil {
				log.Println("[Worker] error scanning request id.", err)
				continue
			}
			_, err := w.postgresClient.Exec(denyQuery, reqId)
			if err != nil {
				log.Println("[Worker] Error sending notify req_deny.", err)
			}
			// and sleep 0.1 second
			time.Sleep(100)
		}
	}()
}
